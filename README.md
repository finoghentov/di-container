# Simple Dependency Injection Container

## Accessing the Container
You can build your container instance via static method:
```
$container = Container::init();
```
Container instance is singleton by default and rebuilding it won't give you new instance.

To obtain created Container instance you can use statis `getInstance` method:

```
$container = Container::getInstance();
```

If you really want to create new Container instance use:
```
Container::unsetInstance();
$container = Container::init();
```
## Basic Usage
You can resolve any instance with any dependencies in it using public `make` method.
This method will resolve all dependencies in reverse and all default parameters in it.

### Example:
```
class A {
    public B $b;
    
    public function __construct(B $b, $default = 'default') {
        $this->b = $b;
    }
    
    public function doSomeStuffWithDependency()
    {
        $this->b->doSomething();
    }
}

$container = Container::init();
$instance = $container->make(A::class); // Will resolve A class with B dependency
$instance->doSomeStuffWithDependency();
```

You can also set you parameters instead of default using name of parameter:

``` 
$instance = $container->make(A::class, [
    'default' => 'Not default parameter'
]); 
```

## Bindings and Singletons

### Bindings

You can bind interface to real realization using `bind` method:

``` 
$container->bind(IRequest::class, HttpRequest::class);
```

After this each class that uses `IRequest` as dependency will get `HttpRequest` resolving via DI:

```
$container->make(IRequest::class); // Instance of HttpRequest
```

Also you can bind custom callback function instead of concrete realisation:
``` 
$container->bind(IRequest::class, function(Container $container){
    return $container->make(HttpRequest:class);
});
```

### Singletons

You can bind dependency with third `singleton` parameter if you want to make 
the singleton instance 

``` 
$container->bind(IRequest::class, HttpRequest::class, true);

// or quicker method

$container->singleton(IRequest::class, HttpRequest::class);
```

You can check if there is resolved singleton instance using 
`has` method and obtain it using `get` method:

``` 
$container->has(IRequest::class); // false
$container->get(IRequest:class); // null

$container->singleton(IRequest::class, HttpRequest::class);

$container->has(IRequest::class); // true
$container->get(IRequest:class); // HttpRequest instance
```

### Aliases

You can make aliases for specific realizations or abstracts:
``` 
$container->alias('request', HttpRequest::class);
$container->make('request'); // HttpRequest instance
```

## Class Method Resolving

It is possible to call a class method with all dependency in it via DI Container.
Method `call` will create a class instance and run a method that you set injecting 
all dependencies.

``` 
class Controller {
    public function someAction(HttpRequest $request, $id = 0) {
        // some stuff
    }
}

$container = Container::init();

$result = $container->call([
    Controller::class, 'someAction'
]); // resulf of method completing
```

You can substitute parameters depends on what you need. For Example:

``` 
$result = $container->call([
    Controller::class, 'someAction'
], [
    'id' => $_GET['id']
]);
```

You can pass parameters for instance that will call action via 3rd parameter:

```
$result = $container->call([
    Controller::class, 'someAction'
], [], [
    'service' => Service::class
]);
```

## Testing

You can run phpunit tests via:
``` 
$ vendor/bin/phpunit
```
