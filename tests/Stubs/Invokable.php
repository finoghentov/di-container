<?php

namespace Finoghentov\Container\Tests\Stubs;

class Invokable
{
    public function __invoke(Request $request): array
    {
        return [$request, 'invoked'];
    }
}
