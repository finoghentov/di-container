<?php

namespace Finoghentov\Container\Tests\Stubs;

class DatabaseDriver
{
    /**
     * @var Request
     */
    private Request $request;

    /**
     * DatabaseDriver constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
