<?php

namespace Finoghentov\Container\Tests\Stubs;

use Finoghentov\Container\Tests\Stubs\Contracts\IRequest;

class Controller
{
    /**
     * @var IRequest
     */
    private IRequest $request;

    /**
     * Controller constructor.
     * @param IRequest $request
     */
    public function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    /**
     * @param IRequest $request
     * @param mixed $mixed
     * @return array
     */
    public function action(IRequest $request, $mixed = 'string'): array
    {
        return [$request, $mixed, $this->request];
    }
}
