<?php

namespace Finoghentov\Container\Tests\Stubs;

use Finoghentov\Container\Tests\Stubs\Contracts\IRequest;
use Finoghentov\Container\Tests\Stubs\Contracts\IResponse;

class Application
{
    /**
     * @var IRequest
     */
    private IRequest $request;

    /**
     * @var IResponse
     */
    private IResponse $response;

    /**
     * JsonResponse constructor.
     *
     * @param IResponse $response
     * @param IRequest $request
     */
    public function __construct(
        IResponse $response,
        IRequest $request
    ) {
        $this->response = $response;
        $this->request = $request;
    }

    /**
     * @return IResponse
     */
    public function getResponse(): IResponse
    {
        return $this->response;
    }

    /**
     * @return IRequest
     */
    public function getRequest(): IRequest
    {
        return $this->request;
    }

    public function getResponseStatus(): int
    {
        return $this->response->getStatus();
    }
}
