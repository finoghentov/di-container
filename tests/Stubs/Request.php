<?php

namespace Finoghentov\Container\Tests\Stubs;

use Finoghentov\Container\Tests\Stubs\Contracts\IRequest;

class Request implements IRequest
{
    /**
     * Get all data from request.
     */
    public function all(): array
    {
        // TODO: Implement all() method.
    }

    /**
     * Get request url.
     */
    public function url(): string
    {
        // TODO: Implement url() method.
    }
}
