<?php

namespace Finoghentov\Container\Contracts;

use Closure;
use Finoghentov\Container\Exceptions\BindingException;
use ReflectionException;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    /**
     * @param string $abstract
     * @param array $parameters
     * @return mixed
     * @throws ReflectionException|BindingException
     */
    public function make(string $abstract, array $parameters = []);

    /**
     * @param string $abstract
     * @param mixed $instance
     */
    public function instance(string $abstract, $instance): void;

    /**
     * @param string $alias
     * @param string $abstract
     */
    public function alias(string $alias, string $abstract): void;

    /**
     * @param $abstract
     * @return mixed
     */
    public function getAlias($abstract);

    /**
     * @param string $abstract
     * @param Closure|string|null $concrete
     * @param bool $singleton
     */
    public function bind(string $abstract, $concrete = null, bool $singleton = false): void;

    /**
     * @param $concrete
     * @return mixed
     * @throws BindingException
     * @throws ReflectionException
     */
    public function build($concrete);

    /**
     * @param Closure|string|array $closure
     * @param array $parameters
     * @param array $instance_parameters
     * @return mixed
     */
    public function call($closure, array $parameters = [], array $instance_parameters = []);
}
